create table employee
(
    id         int primary key auto_increment not null,
    nama       varchar(50),
    atasan_id  int,
    company_id int not null,
    foreign key fk_company (company_id) references company (id)
) engine = InnoDB;

create table company
(
    id     int not null primary key,
    nama   varchar(50),
    alamat varchar(50)
) engine = InnoDB;

insert into company (id, nama, alamat)
VALUES (1, 'PT JAVAN', 'SLEMAN'),
       (2, 'PT DICODING', 'BANDUNG');
insert into employee (nama, atasan_id, company_id)
VALUES ('Pak Budi', null, 1),
       ('Pak Tono', 1, 1),
       ('Pak Totok', 1, 1),
       ('Bu Sinta', 2, 1),
       ('Bu Novi', 3, 1),
       ('Andre', 4, 1),
       ('Dono', 4, 1),
       ('Ismir', 5, 1),
       ('Anto', 5, 1);

select *
from employee
where id = 1;

select *
from employee
where atasan_id in (5, 4);

select *
from employee
where atasan_id = 1;

select *
from employee
where atasan_id in (2, 3);


select concat('Bawahan Pak Budi berjumlah ', count(atasan_id), ' orang')
from employee
where atasan_id is not null;
select concat('Bawahan Pak Tono berjumlah ', count(atasan_id), ' orang')
from employee
where atasan_id in (2, 4, 5);
select concat('Bawahan Pak Totok berjumlah ', count(atasan_id), ' orang')
from employee
where atasan_id in (3, 4, 5);
select concat('Bawahan Bu Sinta berjumlah ', count(atasan_id), ' orang')
from employee
where atasan_id in (4);
select concat('Bawahan Bu Novi berjumlah ', count(atasan_id), ' orang')
from employee
where atasan_id in (5);















