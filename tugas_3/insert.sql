create database sdm;
use sdm;

CREATE TABLE karyawan
(
    id            int not null,
    nama          varchar(50),
    jenis_kelamin varchar(2),
    status        varchar(10),
    tanggal_lahir date,
    tanggal_masuk date,
    Departemen    int,
    primary key (id)
);

ALTER TABLE karyawan
    MODIFY id int auto_increment not null;


CREATE TABLE departemen
(
    id   int not null auto_increment primary key,
    nama varchar(20)
);

INSERT INTO karyawan
(nama,
 jenis_kelamin,
 status,
 tanggal_lahir,
 tanggal_masuk,
 Departemen)
VALUES ('Rizki Saputra',
        'L',
        'Menikah',
        '1980-11-10',
        '2011-1-1',
        1),
       ('Farhan Reza',
        'L',
        'Belum',
        '1989-11-1',
        '2011-1-1',
        1),
       ('Riyando Adi',
        'L',
        'Menikah',
        '1977-1-25',
        '2011-1-1',
        1),
       ('Diego Manuel',
        'L',
        'Menikah',
        '1983-2-22',
        '2012-4-9',
        2),
       ('Satya Laksana',
        'L',
        'Menikah',
        '1981-12-1',
        '2011-3-19',
        2),
       ('Miguel Hernandez',
        'L',
        'Menikah',
        '1994-10-16',
        '2014-6-15',
        2),
       ('Putri Persada',
        'P',
        'Menikah',
        '1988-1-30',
        '2013-4-14',
        2);

INSERT INTO karyawan
(nama,
 jenis_kelamin,
 status,
 tanggal_lahir,
 tanggal_masuk,
 Departemen)
VALUES ('Alma Safira',
        'P',
        'Menikah',
        '1991-5-18',
        '2013-9-28',
        3),
       ('Haqi Hafiz',
        'L',
        'Belum',
        '1995-9-19',
        '2015-3-9',
        3),
       ('Abi Isyawara',
        'L',
        'Belum',
        '1991-6-3',
        '2012-1-22',
        3),
       ('Maman Kresna',
        'L',
        'Belum',
        '1993-8-21',
        '2012-9-15',
        3),
       ('Nadia Aulia',
        'P',
        'Belum',
        '1989-10-7',
        '2012-5-7',
        4),
       ('Mutiara Rezki',
        'L',
        'Menikah',
        '1988-3-23',
        '2013-5-21',
        4),
       ('Dani Setiawan',
        'L',
        'Belum',
        '1986-2-11',
        '2014-11-30',
        4),
       ('Budi Putra',
        'L',
        'Belum',
        '1995-10-23',
        '2015-12-3',
        4);

INSERT INTO departemen
(nama)
VALUES ('Manajemen'),
       ('Pengembangan Bisnis'),
       ('Teknisi'),
       ('Analis');

ALTER TABLE karyawan ADD FOREIGN KEY (Departemen) REFERENCES departemen(id);

